## RECALBOX - SYSTEM AMIGA CD32 ##

Placez ici vos roms amiga CD32.

Les roms doivent être:
- Images ISO (*.iso avec eventuellement un .cue correspondant)
- Images BIN (*.bin avec eventuellement un .cue correspondant)
Les deux peuvent être zippés (Un seul fichier zip par iso/bin avec eventuellement le .cue)

Des fichiers UAE (*.uae) peuvent être utilisés pour specifier une configuration particulière pour un jeu.

Ce système nécessite des bios pour fonctionner (cd32.rom et cd32ext.com).

Consultez la documentation à cette adresse : 
https://github.com/recalbox/recalbox-os/wiki/Amiga-sur-Recalbox-(FR)
